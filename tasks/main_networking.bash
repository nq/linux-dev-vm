#!/usr/bin/env bash
echo "supersede domain-name-servers 192.168.111.2;
supersede routers 192.168.111.2;" | sudo tee -a /etc/dhcp/dhclient.conf

echo "
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface

# The primary network interface
allow-hotplug eth0
auto lo
iface lo inet loopback
iface eth0 inet dhcp
#dns-nameserver 10.0.2.2
pre-up sleep 2

auto eth1
iface eth1 inet static
      address 192.168.111.3
      netmask 255.255.255.0
      gateway 192.168.111.2" | sudo tee /etc/network/interfaces

#sudo service networking restart