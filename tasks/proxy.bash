#!/usr/bin/env bash

http_proxy=http://10.0.2.2:3128
cat >> /etc/environment << EOL
http_proxy=$http_proxy
https_proxy=$http_proxy
HTTP_PROXY=$http_proxy
HTTPS_PROXY=$http_proxy
no_proxy=localhost
NO_PROXY=localhost
EOL
