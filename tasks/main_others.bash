#!/usr/bin/env bash

### First, update and upgrade.
sudo apt-get update
sudo apt-get upgrade -y

### Docker
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

sudo apt-get update
sudo apt-get install -y docker-ce
sudo usermod -aG docker $USER

### Java
sudo apt-get install -y openjdk-8-jdk openjdk-8-source
sudo apt-get install -y ca-certificates-java

export JAVA_HOME="$(dirname $(dirname $(readlink -f $(which javac))))"
echo "JAVA_HOME=$JAVA_HOME" | sudo tee -a /etc/environment

### VirtualBox Extensions
sudo apt-get purge virtualbox-guest-dkms
sudo apt-get install -y linux-headers-$(uname -r) build-essential dkms
wget --progress=bar:force http://download.virtualbox.org/virtualbox/5.2.20/VBoxGuestAdditions_5.2.20.iso
sudo mkdir /media/VBoxGuestAdditions
sudo mount -o loop,ro VBoxGuestAdditions_5.2.20.iso /media/VBoxGuestAdditions
sudo sh /media/VBoxGuestAdditions/VBoxLinuxAdditions.run uninstall --force
sudo sh /media/VBoxGuestAdditions/VBoxLinuxAdditions.run
rm VBoxGuestAdditions_5.2.20.iso
sudo umount /media/VBoxGuestAdditions
sudo rmdir /media/VBoxGuestAdditions
sudo usermod -aG vboxsf $USER

### Desktop
sudo apt-get install -y ubuntu-desktop

### Gradle
wget --progress=bar:force "https://services.gradle.org/distributions/gradle-4.8-all.zip"
unzip -q gradle-*.zip
rm gradle-*.zip
sudo mkdir -p /opt
sudo mv gradle-* /opt
export GRADLE_HOME=(/opt/gradle-*)
echo "GRADLE_HOME=$GRADLE_HOME" | sudo tee -a /etc/environment
mkdir -p $HOME/.gradle
echo "exec $GRADLE_HOME/bin/gradle \$@" | sudo tee /usr/local/bin/gradle
sudo chmod +x /usr/local/bin/gradle

### OpenShift tools
wget --progress=bar:force https://repository.paychex.com/artifactory/Platform_Engineering/openshift/3.9/oc-3.9.40-linux.tar.gz
tar -xzvf oc-*-linux.tar.gz
rm oc-*-linux.tar.gz
sudo mv oc /usr/local/bin/

### Firefox
sudo apt-get install firefox p11-kit
sudo dpkg-divert --divert /usr/lib/firefox/libnssckbi.so.orig --rename /usr/lib/firefox/libnssckbi.so --local
sudo ln -s /usr/lib/x86_64-linux-gnu/pkcs11/p11-kit-trust.so /usr/lib/firefox/libnssckbi.so

### Golang
curl https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go*.tar.gz
sudo ln -s /usr/local/go/bin/go /usr/local/bin/go
sudo ln -s /usr/local/go/bin/godoc /usr/local/bin/godoc
sudo ln -s /usr/local/go/bin/gofmt /usr/local/bin/gofmt

### Powershell
wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt-get update
sudo apt-get install -y powershell
rm packages-microsoft-prod.deb

### Maven
wget -q http://www.trieuvan.com/apache/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz
tar -xzvf apache-maven-*.tar.gz
rm apache-maven-*.tar.gz
sudo mv apache-maven-* /opt
sudo ln -s /opt/apache-maven-*/bin/mvn /usr/local/bin
sudo ln -s /opt/apache-maven-*/bin/mvnDebug /usr/local/bin
sudo ln -s /opt/apache-maven-*/bin/mvnjp /usr/local/bin

### Auto login
sudo mkdir -p /etc/lightdm/lightdm.conf.d/
echo "
[SeatDefaults]
autologin-user=vagrant
" | sudo tee -a /etc/lightdm/lightdm.conf.d/50-autologin.conf

### Misc
sudo apt-get install -y dos2unix htop jq unity-tweak-tool shutter
curl -fsSL "https://raw.githubusercontent.com/cykerway/complete-alias/master/completions/bash_completion.sh" >> ~/.bash_completion
echo "LANG=en_US.UTF-8" | sudo tee -a /etc/default/locale
#sudo -u gdm dbus-launch gsettings set set org.gnome.desktop.session idle-delay 0
#sudo -u gdm dbus-launch gsettings set org.gnome.desktop.screensaver lock-enabled false
#gsettings set org.gnome.desktop.session idle-delay 0