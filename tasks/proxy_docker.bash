#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

sudo apt-get update
sudo apt-get install -y docker-ce
sudo usermod -aG docker $USER

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo touch /etc/systemd/system/docker.service.d/http-proxy.conf

echo "[Service]
Environment=\"HTTP_PROXY=$http_proxy\" \"HTTPS_PROXY=$http_proxy\"
" | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf
sudo systemctl daemon-reload
sudo systemctl restart docker

#sudo bash -c "echo listen-address=$(ip -4 addr show dev docker0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}') > /etc/NetworkManager/dnsmasq.d/docker-bridge"
#sudo systemctl reload NetworkManager
#sudo bash -c 'echo -e "{\n\t\"dns\": [\"$(ip -4 addr show dev docker0 | grep -oP "(?<=inet\s)\d+(\.\d+){3}")\"]\n}" > /etc/docker/daemon.json'
#sudo systemctl restart docker
