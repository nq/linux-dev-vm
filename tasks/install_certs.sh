#!/usr/bin/env bash
set -e
set -o pipefail

cert_dir=$(download-certs)
cd $cert_dir

rm -rf /usr/local/share/ca-certificates/paychex
mkdir /usr/local/share/ca-certificates/paychex
cp *.crt /usr/local/share/ca-certificates/paychex/
chmod 755 /usr/local/share/ca-certificates/paychex
chmod 644 /usr/local/share/ca-certificates/paychex/*.crt
update-ca-certificates

cd -
rm -rf $cert_dir

dpkg-divert --divert /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so.orig --rename /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so --local
apt-get update && apt-get install -y libnss3 p11-kit
ln -s /usr/lib/x86_64-linux-gnu/pkcs11/p11-kit-trust.so /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so
