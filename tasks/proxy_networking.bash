#!/usr/bin/env bash

docker run -d -e http_proxy --restart always --net=host --privileged wheelerlaw/redsocks -a 192.168.111.2

cert_dir=$(download-certs)
cd $cert_dir
chmod 644 *.crt
docker build . -f /usr/local/src/https-dns-proxy.Dockerfile -t payx/https-dns-proxy --build-arg http_proxy
cd -
rm -rf $cert_dir

docker run -d -e http_proxy --restart always --net=host --privileged payx/https-dns-proxy

docker run -d -v /etc/octodns/:/etc/octodns/ --restart always --net=host --privileged wheelerlaw/octodns octodns -c /etc/octodns/config.json

