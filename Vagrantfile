# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.define "proxy" do |proxy|
    proxy.vm.box = "ubuntu/xenial64"
    proxy.vm.hostname = "wlaw-proxy"
    proxy.vm.network "private_network", ip: "192.168.111.2", virtualbox__intnet: "mylocalnet", auto_config: true

    proxy.vm.provision "shell", path: "tasks/proxy.bash"
    proxy.vm.provision :reload

    proxy.vm.provision "file", source: "rootfs", destination: "$HOME/rootfs"
    proxy.vm.provision "shell", privileged: false, path: "tasks/rootfs.bash"
    proxy.vm.provision "shell", path: "tasks/install_certs.sh"
    proxy.vm.provision :reload

    proxy.vm.provision "shell", privileged: false, path: "tasks/proxy_docker.bash"
    proxy.vm.provision "shell", path: "tasks/proxy_networking.bash"
  end

  config.vm.define "main", primary: true do |main|
    main.vm.provider "virtualbox" do |vb|
      # vb.name = "LinuxDev"
      vb.gui = true
      vb.memory = "8184"
      vb.cpus = 4
      vb.customize ['modifyvm', :id, '--clipboard', 'bidirectional']
    end

    main.vm.box = "generic/ubuntu1604"
    main.disksize.size = '50GB'
    main.vm.network "private_network", ip: "192.168.111.3", virtualbox__intnet: "mylocalnet", auto_config: false
    main.vm.provision "shell", path: "tasks/main_networking.bash"
    main.vm.provision :reload

    main.vm.provision "file", source: "rootfs", destination: "$HOME/rootfs"
    main.vm.provision "shell", privileged: false, path: "tasks/rootfs.bash"
    main.vm.provision "shell", path: "tasks/install_certs.sh"
    main.vm.provision "shell", privileged: false, path: "tasks/main_others.bash"
    main.vm.provision "shell", inline: "shutdown now"

    main.vm.synced_folder "./", "/vagrant"

    # Set the boot timeout to 10 minutes, because on the next reboot there are some interactive configuration steps.
    # main.vm.boot_timeout = 600
    # main.vm.provision :reload
  end
end
