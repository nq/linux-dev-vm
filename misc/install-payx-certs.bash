#!/usr/bin/env bash
set -e
set -o pipefail

function getCertChain() {
	curl -fsSL https://code.paychex.com/projects/PUPMOD/repos/payx_linux/raw/files/paychex-ca.pem
}

if ! which curl &>/dev/null; then
	echo '`curl` not found in PATH and is required for this script to work. Exiting...'
	exit 1
fi

if ! which csplit &>/dev/null; then
	echo '`csplit` not found in PATH and is required for this script to work. Exiting...'
	exit 1
fi

if ! which update-ca-certificates &>/dev/null; then
	echo '`update-ca-certificates` not found in PATH and is required for this script to work. Exiting...'
	exit 1
fi

TMPDIR=/var/tmp
export cert_dir=$(mktemp -d -t certs.XXXXXXXXX)

cd $cert_dir

getCertChain | csplit -s -f paychex-indv-cert- - '/-----BEGIN CERTIFICATE-----/' '{*}'

[[ `cat *-00` -eq '' ]] && rm *-00

for cert in paychex-indv-cert-*
do
	new_file_name=`openssl x509 -noout -subject -nameopt multiline -in "$cert" | sed -n 's/ *commonName *= //p'`
	new_file_name="${new_file_name// /_}"

	# Handle non-numbered certs by numbering them.
	# If there is only one other cert (that is likely not numbered)
	if [[ -e "${new_file_name}.crt" ]]; then
		>&2 echo "${new_file_name}.crt already exists"
		# mv $new_file_name "${new_file_name}-1"
		file_num=2

		# If there are other numbered files. Get the highest one, and increment it.
		if [[ `ls ${new_file_name}-*.crt 2>/dev/null | grep -Eo '\-[0-9]+\.crt$' | grep -o '[0-9]*' | sort | tail -n1` != '' ]]; then
			# List files that share the cert name, get the highest version.
			file_num=`ls ${new_file_name}-*.crt | grep -Eo '\-[0-9]+\.crt$' | grep -o '[0-9]*' | sort | tail -n1`
			file_num=$((file_num + 1))
		fi

		new_file_name="${new_file_name}-${file_num}"
	fi

	>&2 echo "Writing ${new_file_name}.crt"
	mv $cert "${new_file_name}.crt"
done

echo $cert_dir

cd $cert_dir

rm -rf /usr/local/share/ca-certificates/paychex
mkdir /usr/local/share/ca-certificates/paychex
cp *.crt /usr/local/share/ca-certificates/paychex/
chmod 755 /usr/local/share/ca-certificates/paychex
chmod 644 /usr/local/share/ca-certificates/paychex/*.crt
update-ca-certificates

cd -
rm -rf $cert_dir

dpkg-divert --divert /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so.orig --rename /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so --local
apt-get update && apt-get install -y libnss3 p11-kit
ln -s /usr/lib/x86_64-linux-gnu/pkcs11/p11-kit-trust.so /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so
