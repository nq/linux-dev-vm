FROM wheelerlaw/https-dns-proxy:latest

RUN apk update && apk add ca-certificates

COPY ./*.crt /usr/local/share/ca-certificates/

RUN update-ca-certificates