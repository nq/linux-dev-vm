#!/usr/bin/env python3

import shutil
import getpass
import sys
import tempfile
from subprocess import run, PIPE
import os
import re
import argparse
import yaml


def main():
    parser = argparse.ArgumentParser(description="Cntlm update utility")
    parser.add_argument("-a", "--answers", help="Answers to the interactive questions. Top level keys only. Use '-' "
                                                "to read from stdin.")
    parser.add_argument("-c", "--config", help="Location of cntlm.conf. Defaults to /etc/cntlm.conf on Linux or "
                                               "C:/Program Files/Cntlm/cntlm.conf on Windows")
    parser.add_argument("-n", "--no_prompt", action="store_true", help="Don't prompt for questions; instead get the "
                                                                       "answer from the answers file or an existing "
                                                                       "config, and failing if neither can provide an "
                                                                       "answer")
    args = parser.parse_args()

    # fd = os.open('/dev/tty', os.O_RDWR|os.O_NOCTTY)
    # tty = io.FileIO(fd, 'w+')
    # stack.enter_context(tty)
    # input = io.TextIOWrapper(tty)

    if args.answers == '-':
        answers = yaml.load(sys.stdin)
        sys.stdin = open("/dev/tty")
    elif args.answers is None:
        answers = None
    else:
        with open(args.answers, "r") as answers_file:
            answers = yaml.load(answers_file)

    if args.config is None:
        cntlm_conf_path = get_default_cntlm_conf_path()
    else:
        cntlm_conf_path = args.config

    cntlm_path = get_cntlm_path()

    if cntlm_path is None:
        print("Error: Cntlm MUST be installed first. Exiting", file=sys.stderr)
        exit(1)

    print("Found Cntlm at:", cntlm_path)

    cntlm_conf = parse_cntlm_conf(cntlm_conf_path)
    if cntlm_conf is None:
        print("Cntlm config empty or doesn't exist. Creating new Cntlm config: " + cntlm_conf_path)
        cntlm_conf = {"Username": "", "Domain": "", "Password": "", "Proxy": ""}

    for cntlm_key in cntlm_conf:
        if answers is not None and cntlm_key in answers:
            cntlm_conf[cntlm_key] = answers[cntlm_key]
        else:
            if args.no_prompt:
                if cntlm_conf[cntlm_key] is None or cntlm_conf[cntlm_key] == "":
                    print("Error: No value found for " + cntlm_key + ". Exiting.", file=sys.stderr)
                    exit(2+127)
            else:
                response = input(cntlm_key + " [" + cntlm_conf[cntlm_key] + "]: ")
                if response != "":
                    cntlm_conf[cntlm_key] = response

    if not args.no_prompt and (answers is None or "Password" not in answers):
        cntlm_conf["Password"] = getpass.getpass("Password: ")

    auth_type = None
    auth_value = None

    if "Password" in cntlm_conf and cntlm_conf["Password"] != "":
        auth_type = "Password"
        auth_value = cntlm_conf["Password"]
    else:
        auth_creds = {
            auth_key: cntlm_conf[auth_key] for auth_key in cntlm_conf
            if auth_key.startswith("Pass") and auth_key != "Password"
        }

        for auth_key in auth_creds:
            if auth_creds[auth_key] is not None and auth_creds[auth_key] != "":
                auth_type = auth_key
                auth_value = auth_creds[auth_key]
                break

    if auth_type is None or auth_type == "" or auth_value is None or auth_value == "":
        print("Error: No authentication credentials found. Exiting.", file=sys.stderr)
        exit(2+127)

    with tempfile.TemporaryDirectory() as dirname:
        temp_cntlm_conf_path = os.path.join(dirname, "cntlm.conf")

        write_cntlm_conf(temp_cntlm_conf_path, cntlm_conf)

        auth_type, auth_value = get_new_cntlm_creds(cntlm_path, temp_cntlm_conf_path, auth_type, auth_value)
        print(auth_type)
        if auth_type is None:
            exit(3+127)

    new_cntlm_conf = {auth_key: cntlm_conf[auth_key] for auth_key in cntlm_conf if not auth_key.startswith("Pass")}
    new_cntlm_conf[auth_type] = auth_value

    write_cntlm_conf(cntlm_conf_path, new_cntlm_conf)

    print("Now restart Cntlm: ")
    print("  Windows: net stop Cntlm")
    print("           net stop Cntlm")
    print("  Ubuntu: sudo service cntlm restart")
    print("  SystemD: sudo systemctl restart cntlm")
    exit(0)


def get_new_cntlm_creds(cntlm_path, conf_path, auth_type, auth_value):
    if auth_type == "Password":
        password = auth_value
    else:
        password = ""

    cmd = [cntlm_path, "-c", conf_path, "-f", "-I", "-M", "http://www.google.com"]
    p = run(cmd, stdout=PIPE, input=password+"\n", encoding="ascii")
    lines = p.stdout.split("\n")

    lines = [line for line in lines if "Password" not in line]

    results = list(filter(re.compile("Pass(?!word).*").match, lines))

    if len(results) == 0:
        print("\n".join(lines))
        return None, None
    auth = results[0].split()
    return auth[0], auth[1]


def get_default_cntlm_conf_path():
    if sys.platform == "win32" and 'PROGRAMFILES(X86)' in os.environ:
        return os.path.join(os.environ['PROGRAMFILES(X86)'], 'Cntlm', 'cntlm.ini')
    elif sys.platform == "win32" and 'PROGRAMFILES' in os.environ:
        return os.path.join(os.environ['PROGRAMFILES'], 'Cntlm', 'cntlm.ini')
    else:
        return os.path.join(os.path.abspath(os.sep), 'etc', 'cntlm.conf')


def get_cntlm_path():
    cntlm_path = shutil.which("cntlm")
    if cntlm_path is None:

        win_x86_64_cntlm_path = os.path.join(os.environ['PROGRAMFILES(X86)'], 'Cntlm', 'cntlm.exe')
        if os.path.isfile(win_x86_64_cntlm_path):
            cntlm_path = win_x86_64_cntlm_path

        win_x86_cntlm_path = os.path.join(os.environ['PROGRAMFILES'], 'Cntlm', 'cntlm.exe')
        if os.path.isfile(win_x86_cntlm_path):
            cntlm_path = win_x86_cntlm_path

    return cntlm_path


def parse_cntlm_conf(cntlm_conf_path):
    data = {}
    if cntlm_conf_path is None or not os.path.exists(cntlm_conf_path):
        return data
    with open(cntlm_conf_path, 'r') as cntlm_conf:
        p = re.compile(r"^([^#\s]*)[^\S\x0a\x0d]+(.*)$")
        for line in cntlm_conf:
            s = p.search(line)
            if s:
                key = s.group(1)
                value = s.group(2)
                data[key] = value
    return data


def write_cntlm_conf(cntlm_conf_path, data: dict):
    with open(cntlm_conf_path, 'w') as cntlm_conf:
        for data_key in data:
            if data_key != "Password":
                cntlm_conf.write(data_key + "\t" + data[data_key] + os.linesep)


main()
