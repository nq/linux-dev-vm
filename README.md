_Originally authored by Wheeler Law_

# Ultimate Linux Development VM

> You've stumbled upon a door where your mind is the key. There are none who will lend you guidance; 
these trials are yours to conquer alone. Entering here will take more than mere logic and strategy, 
but the criteria are just as hidden as what they reveal. Find yourself, and you will find the very 
thing hidden within this repo. Beyond here is something like a utopia -- beyond here is what development ought to be.
>
>This is a mirage. 

Welcome to official git repository for the *Ultimate Linux Development VM*! 

Like me, you are probably in disgust of the rather pathetic state of development here at Paychex. Certain folks in this
wonderful organization have managed to pull off the rather amazing feat of making development tasks so astoundingly difficult that 
it would be easier to quit your job, start a competing company, steal all of Paychex's market share, and force it
into chapter eleven bankruptcy (I know this probably isn't likely, but hey, a man can dream). I won't name names of who
these people are because I don't want to steal their thunder. 

Some of the really cool things that these people have done include (but of course are not limited to):
* No internet DNS resolution, because if you can resolve internal *and* external hostnames, then the Russians can too. 
* Non-transparent, intercepting HTTP proxies (probably invented by the same person who created Windows ME)
* Prohibition of ANY local operating system other than those that are hand made by Desktop Engineering's lord and savior, Bill Gates (open source is for commies anyway)

If you don't understand why the above things make development miserable, then you're likely part of the problem. 

If you have arrived here in search of redemption, then I have some good news. I have managed to pull of an even more amazing feat 
of crafting this amazing VM *that doesn't require configuring proxy settings*. How have I done this, you might ask? Read on, and you shall find out. 

## How It Works

There are a few different components to this project, all working in harmony to deliver you the best possible development experience
given the handsome constraints that have been placed upon us. 

### The Main Development VM

There is nothing really special about this VM. Because of the components below, you do not need to configure proxy settings 
in this VM; in essence, it works as if you had direct access to the internet *and* the internal network. 

### The Gateway VM

This is a headless VM that does most of the fancy routing of traffic from the main development VM (which is configured 
to send network traffic directly to the gateway VM) In this VM, there are three services running that handle and route this traffic,
each serving a distinct purpose: 

* **Octodns**: a custom DNS resolver running inside of a Docker container that multiplexes DNS requests to either the system default DNS
server, or DNS servers on the internet. Octodns had to be custom witten because of the differences in the way
Paychex has configured its DNS servers, and the way DNS was designed to be configured. Essentially, DNS was designed such that,
even on a private network with no internet access to the outside world, hostnames outside of this network could still be resolved. 
Paychex, however, does not abide by this, meaning that the resolution of an internet hostname would fail (NXDOMAIN). Compliant 
DNS resolvers are designed with this assumption in mind, and so even if you have multiple upstream resolvers configured for
a given DNS server, resolution will fail if *any* of the upstream resolvers fail to resolve the name, even if there are more
upstream resolvers that haven't been tried yet. Octodns does things a little differently, by trying all configured upstream
resolvers until either resolution succeeds for one of them, or fails for all of them. 

* **Https_dns_proxy**: another custom DNS resolver that simply converts TCP DNS requests to HTTPS DNS requests
(yes, that is a [thing](https://en.wikipedia.org/wiki/DNS_over_HTTPS)). This resolver passes these DNS requests over HTTPS
(to get through the HTTP proxies) to [Google's](https://developers.google.com/speed/public-dns/docs/dns-over-https) DNS servers.
This is how Octodns is able to multiplex internal DNS requests and internet DNS requests. 

* **Redsocks**: a transparent TCP-to-proxy redirector, also running in a Docker container. At this point, the application 
has resolved the address of a domain name, and is ready to start a connection. This is where Redsocks comes in. It listens for normal
TCP connections, and when one is received, it connects to an upstream HTTP proxy and opens a connection between it 
and the destination server using the [HTTP CONNECT method](https://en.wikipedia.org/wiki/HTTP_tunnel#HTTP_CONNECT_method), 
*exactly like your browser would if you had configured its HTTP proxy settings*.  

### Your Local Machine

The final piece to the puzzle is a locally-running proxy authenticator that allows an application--that doesn't speak NTLM--to
connect through an HTTP proxy that must be authenticated with using NTLM. Because Redsocks does not speak NTLM, a
local proxy authenticator solves this issue. There are two well known proxy authenticators:

* **Cntlm**: a proxy authenticator that is written in C and is designed to authenticate a connection to a proxy 
server on behalf of an application. It was originally written quite some time ago, but is still maintained and is quite fast due
to it being written in C. It is also cross platform, and can be compiled against a number of different operating systems and 
architectures. However, Cntlm lacks the ability to utilize the auto-configuration script that is configured in your Internet Options,
meaning you have to define the proxy server manually. This is problematic when you are outside of the network and aren't 
connected with the VPN, which means you need to then reconfigure your applications to talk directly to the internet. This 
reconfiguration needs to happen every time you enter and leave the Paychex network. 

* **Px**: an alternative to Cntlm, written in Python, and is not only capable of using the auto-configuration script, but
the NTLM credentials are automatically retrieved from Windows using the Windows SSPI API. This means that, unlike Cntlm,
you don't need to store any credentials for Px to be able to authenticate with the proxy server. However, the main drawback 
of Px is that there are some minor performance issues. 

## How To Use

In order to be able to achieve the full glory of uninterrupted prosperity, you will need to do the following:

* Download and install a local proxy authenticator. I recommend [Px](https://github.com/genotrance/px) because of how easy
it is to setup:
```bash
pip install git+https://github.com/genotrance/px
px --gateway --threads=50
```

* [Install VirtualBox](https://www.virtualbox.org/wiki/Downloads) and enable hardware virtualization support in your BIOS. 
The VM images currently only work with VirtualBox, but I am planning on getting them to work with other hypervisors in 
the future. **Note**: because these images are running in VirtualBox, this means **you need to boot with Hyper-V support 
disabled** (see [this](https://forums.virtualbox.org/viewtopic.php?f=6&t=41258#p236620) post on how to do so).

* [Download and install Vagrant](https://www.vagrantup.com/downloads.html).

* Install the necessary Vagrant plugins by running the following commands:
```bash
vagrant plugin install vagrant-disksize
vagrant plugin install vagrant-reload 
``` 

* Clone this repository onto your local machine:
```bash
git clone https://wlaw@code.paychex.com/scm/~wlaw/linux-dev-vm.git
```

* Start up the VMs, and sit back and relax while they configure themselves:
```bash
cd linux-dev-vm
vagrant up
```

The initial provisioning and configuration should take about twenty to thirty minutes. Once it has completed, the
main development machine will shut down, which can then be started by either running `vagrant up main` or by looking for 
the LinuxDev machine in VirtualBox. 

Both the username _and_ password for both VMs is just `vagrant`


## Included Software

This VM contains the following software:

- Git
- Gradle (`/opt/gradle-4.9`)
- Java 8 JDK
- Docker

I am currently working to add more things. 


## What Doesn't Work

So at this point you might be thinking that this is the most perfect thing to ever come to fruition here at Paychex. 
However--and I don't mean to be a buzz kill--there *are* a few things that do not work properly:

* Access to anything that requires authentication using `gateway.paychex.com` does not work. This includes ServiceNow,
Oracle Employee Self-service, and some other things. This is because the 
auto-configuration [script](http://sso.paychex.com/pac/pac.asp) erroneously tells connections to `gateway.paychex.com` 
to connect directly rather than through the proxy. This is the line that does it (the mistake is the inclusion of the 
141.123.0.0/16 subnet as an internal address):
```bash
if (/^(10\.|127\.|141\.123\.|172\.(1(6|7|9)|2(0|1|5|6)|3(0))\.|192\.168\.)/.test(host)) {return prno}
```

* Px can sometimes not complete connections for you. Usually, this is just a matter of refreshing the page you are trying
to get to. 

* Because Hyper-V support must be disabled to use VirtualBox, Docker for Windows will no longer function correctly on 
your host machine. Usually this isn't such a huge deal since I have included docker in the main development VM.
 